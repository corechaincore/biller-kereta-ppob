package main

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"log"
	"github.com/go-resty/resty"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"encoding/xml"
	"strconv"
)

type MyRespEnvelope struct {
	XMLName xml.Name
	Body    Body
}

type Body struct {
	XMLName     xml.Name `xml:"Body"`
	KaiCheckBookResponse completeResponse `xml:"kaiCheckBookResponse"`
}

type completeResponse struct {
	XMLName     xml.Name `xml:"kaiCheckBookResponse"`
	Return compReturn `xml:"return"`
}

type MyRespEnvelopeIssued struct {
	XMLName xml.Name
	Body    BodyIssued
}

type BodyIssued struct {
	XMLName     xml.Name `xml:"Body"`
	KaiIssuedResponse completeResponseIssued `xml:"kaiIssuedResponse"`
}

type completeResponseIssued struct {
	XMLName     xml.Name `xml:"kaiIssuedResponse"`
	Return compReturn `xml:"return"`
}

type compReturn struct {
	XMLName xml.Name `xml:"return"`
	ErrCode string `xml:"errCode"`
	TotalPrice string `xml:"totalPrice"`
	Msg string `xml:"msg"`

}

var url = "http://117.102.64.238:1212/index.php"
var username = "bd8446eaa574ab00ab72249f8b06a759"
var password = "a3355920444fae8eb84b1f6304bbe1d9"

func test(w http.ResponseWriter, req *http.Request, _ httprouter.Params){
	msg := ""
	status := 200

	body,_ := ioutil.ReadAll(req.Body)
	code,_ := jsonparser.GetString(body,"bookingcode")

	reqGetPrice :=
		"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
		"xmlns:univ=\"http://www.example.pl/ws/test/universal\"> <soapenv:Header/> " +
		"<soapenv:Body> <wsse:kaiCheckBook soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> " +
		"<bookingCode xsi:type=\"xsd:string\">"+code+"</bookingCode> " +
		"</wsse:kaiCheckBook> </soapenv:Body> " +
		"</soapenv:Envelope>"

		resp, err := resty.R().SetBasicAuth(username,password).
		SetHeader("Content-Type","text/xml").
		SetBody(reqGetPrice).Post(url)
		var response = string(resp.Body())
		resStruct := &MyRespEnvelope{}
		err = xml.Unmarshal([]byte(response), resStruct)

		if err != nil{
			println(err)
			status = 403
		}
		errCode := resStruct.Body.KaiCheckBookResponse.Return.ErrCode

		if errCode != "0" {
			msg = "Invalid Booking Code"
			status = 404
		}else {
			price := resStruct.Body.KaiCheckBookResponse.Return.TotalPrice
			bodyReq := "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
				"xmlns:univ=\"http://www.example.pl/ws/test/universal\"> <soapenv:Header/> " +
				"<soapenv:Body> <wsse:kaiIssued soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> " +
				"<bookingCode xsi:type=\"xsd:string\">" + code + "</bookingCode> " +
				"<totalPrice xsi:type=\"xsd:string\">" + price + "</totalPrice> " +
				"</wsse:kaiIssued> </soapenv:Body> " +
				"</soapenv:Envelope>"
			res, _ := resty.R().SetBasicAuth(username,password).
				SetHeader("Content-Type", "text/xml").
				SetBody(bodyReq).Post(url)
			var valResponse = string(res.Body())
			resStructs := &MyRespEnvelopeIssued{}
			err = xml.Unmarshal([]byte(valResponse), resStructs)
			msg = resStructs.Body.KaiIssuedResponse.Return.Msg
			errCode = resStructs.Body.KaiIssuedResponse.Return.Msg
			if errCode != "0" {
				status = 403
			}
		}
	w.Write([]byte("{\"code\":\""+strconv.Itoa(status)+"\",\"message\":\""+msg+"\"}"))
	w.WriteHeader(status)
}

func main() {
	router := httprouter.New()
	router.POST("/test",test)
	log.Fatal(http.ListenAndServe(":8080", router))
}